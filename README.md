# rpideploy
A perl based script to automate new deployments of Raspberry Pis running Raspberry Pi OS.

# Intended usage

Intended usage is take a fresh install of Raspberry PI OS and help automate steps you need to run
to deploy it. I use it to create a standard image with clonezilla that I can then copy to however
many MicroSD cards I need.

# Notes:
 Creation of the steps is best done on a existing Raspberry PI with the same version on Perl as will be present on
 the new system. The reason is while the steps file itself is originally plain text, it is converted into a processed
 form using stepper.pl. The stepper.pl outputs a file named stpcomp, which you will want to drop into the /etc folder 
 of the newly flashed microsd card before booting the RPI for the first time. Please note this script can hook into
 the startup of your Raspberry PI if desired, giving it additional flexibility beyond just deploying a Raspberry PI.
 Core functionality works, though some code present will be expanded upon for additional features.
 
# Usage 

perl bootup.pl <setup | run | remove > 

setup - this switch causes the script to embed itself into the rc.local file to run on reboot/boot. 

run - this causes it open /etc/stpcomp and executes specified steps in the order specified

remove - this causes the script to remove itself from rc.local

General usage steps are as follows.

1. Create a steps file. One command per line, steps can contain any number of steps.
2. From a similar Raspberry PI run perl stepper.pl, this creates the stpcomp file.
This file you will need to copy to /etc/stpcomp on the target system. 
3. Either embed the script in /etc/rc.local manually or if the system to be deployed is booted up already
then run Perl bootupl.pl setup and let it do its thing.


# Steps file example

step 1
wifi
run apt-get update
run apt-get upgrade -y
step 2
run curl -sSL https://get.docker.com | sh
step 3
run apt install python3-pip -y
run apt install docker-compose -y
reboot

# Steps notes

Please note the info for setting up wifi is found hard coded into the bootup.pl file. You will want to set your
SSID and password there if you intend use this feature. Reboot requires the script to be embeded into the /etc/rc.local
file either by hand or as previously noted.

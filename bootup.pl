#!/usr/bin/perl

# script for pibakery startup stuffs
# or other automated deployment

# hooks into rc.local

use Cwd;
use Storable;

($arg) = @ARGV;
$dir = getcwd;
chomp($arg);
$steps = retrieve('/etc/stpcomp');

# network test is automatic
$result = eval { `ping -c 3 google.com` };
unless($result) {
    # If we get here it failed
    #print $@;
    $isonline = 0;
}else{
    $isonline = 1;
}

if($arg eq "setup"){
    # Yes, this lazy
    $rclocal = `cat /etc/rc.local`;
    $rclocal =~ s/exit 0//g;
    $rclocal .= "\n$^X $dir/$0 run"; # Don't try and comprehend this
    $rclocal .= "\nexit 0;";
    #print "$rclocal";
    open(rct,">rc.local");
    print rct "$rclocal";
    close rct;
    system("cp /etc/rc.local /etc/rc.local.orig");
    system("cp rc.local /etc/rc.local");

    
}

if($arg eq "run"){
    # Prepare for main execution loop
    # main event loop
    for $step ( sort(keys %{ $steps } )) {
   # print "Step # ${$steps->{$step}} ";
   print "Step = $step \n"; 
    for $substep ( sort(keys %{ $steps->{$step} } )) {
       # print "substep = $substep\n";
            for $exeord ( keys %{ $steps->{$step}->{$substep} }){
                $lstep = $step;
                $lsubstep = $substep;
                
                if($steps->{$step}->{$substep}->{$exeord} =~ /reboot/i){
                    # reboot handler
                    print "REBOOT\n";
                    delete $steps->{$step}->{$substep};
                    store \%$steps, 'stpcomp';
                    exec("reboot");
                    exit(0);

                }

                if($steps->{$step}->{$substep}->{$exeord} =~ /wifi/i){
                    open(wifis,">/etc/wpa_supplicant/wpa_supplicant.conf");
                    print wifis "country=US\n";
                    print wifis "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n";
                    print wifis "update_config=1\n";
                    print wifis "\n\n";
                    print wifis "network={\n";
                    print wifis "    ssid=\"yourwifissid\"\n";
                    print wifis "    scan_ssid=1\n";
                    print wifis "    key_mgmt=WPA-PSK\n";
                    print wifis "    psk=\"yourpassword\"\n";
                    print wifis "}\n";
                    close(wifis);
                    delete $steps->{$step}->{$substep};
                }else{
                system("$steps->{$step}->{$substep}->{$exeord}");
                delete $steps->{$step}->{$substep};
                }

            }
        
    }
delete $steps->{$step};    
}
    # end of main event loop
}

if($arg eq "remove"){
     # Yes, this lazy
    $rclocal = `cat /etc/rc.local`;
    $rclocal =~ s/exit 0;//g;
    $rclocal =~ s/$^X $dir\/$0 run//g; # Don't try and comprehend this
    $rclocal .= "\nexit 0;";
    #print "$rclocal";
    open(rct,">rc.local");
    print rct "$rclocal";
    close rct;
    system("cp rc.local /etc/rc.local");
}

